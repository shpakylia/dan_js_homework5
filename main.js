function createNewUser() {
    let firstName = askParam('enter your first name:', 'string');
    let lastName = askParam('enter your last name:', 'string');
    let birthday = askParam('enter your birthday:', 'date', 'XX.XX.XXXX');
    let newUser  = {
        get firstName() {
            return this.firstName;
        },
        get lastName() {
            return this.firstName;
        },
        get birthday(){
          return this.birthday;
        },
        setFirstName: function(updatedName){
            Object.defineProperty(newUser, 'firstName', {
                value: updatedName,
            });
        },
        setLastName: function(updatedLastname){
            Object.defineProperty(newUser, 'lastName', {
                value: updatedLastname,
            });
        },
        setBirthday(updateBirthday){
            let date = validDate(updateBirthday);
            if(date !== false) {
                Object.defineProperty(newUser, 'birthday', {
                    value: date,
                });
                this.birthday = date;
            }
        },
        getLogin(){
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge(){
            let currentDate = new Date();
            return currentDate.getFullYear() - this.birthday.getFullYear();
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        }

    };
    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        enumerable: true,
        configurable: true,
        writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        enumerable: true,
        configurable: true,
        writable: false
    });
    Object.defineProperty(newUser, 'birthday', {
        value: birthday,
        enumerable: true,
        configurable: true,
        writable: false

    });
    return newUser;

    function askParam(msg, type, defaultValue = '') {
        let param;
        let newMsg = msg + ' Does not correct. Try again!';
        let valid = false; //validation

        do{
            param = prompt(msg, defaultValue);
            switch (type) {
                case 'string':
                    if(param) valid = !valid;
                    break;
                case 'date':
                    let date = validDate(param);
                    if(date === false) break;
                    param = date;
                    valid = !valid;
                    break;
            }
            if(!valid){
                msg = newMsg;
            }
        }while(!valid);
        return param;
    }
    function strToDate(str) {
        let splitDate = str.split('.');
        return new Date(splitDate[2], splitDate[1], splitDate[0]);
    }
    function validDate(val) {
        let pattern = '^\\d{2}.\\d{2}.\\d{4}$';
        let regex = new RegExp(pattern);
        if(!regex.test(val)) return false;
        let date = strToDate(val);
        if (date.getTime() > new Date().getTime()) return false;
        return date;
    }

}



let user = createNewUser();
console.log('login: ', user.getLogin());
console.log('age: ', user.getAge());
console.log('password: ', user.getPassword());

user.firstName = 'Juli';
console.log('first name after set: ', user.firstName);

user.setFirstName('BlaBla');
console.log('first name after set with func: ', user.firstName);

user.setBirthday('3244324');
console.log('birthday after set with func but not valid: ', user.birthday);

user.setBirthday('23.01.2000');
console.log('birthday after set with func valid: ', user.birthday);
